const Color = require('color-js');
const express = require('express');
const app = express();

app.options('/*', function(req, res) {
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Headers', 'Content-Type');
    res.status(200).send();
});

app.get('/task2d/', function(req, res) {
	const reHex = /^#?([0-9a-fA-F]{6}|[0-9a-fA-F]{3})$/;
	const reAll = /^(?:#(?:[A-Fa-f0-9]{3}){1,2}|(?:rgb[(](?:\s*0*(?:\d\d?(?:\.\d+)?(?:\s*%)?|\.\d+\s*%|100(?:\.0*)?\s*%|(?:1\d\d|2[0-4]\d|25[0-5])(?:\.\d+)?)\s*(?:,(?![)])|(?=[)]))){3}|hsl[(]\s*0*(?:[12]?\d{1,2}|3(?:[0-5]\d|60))\s*(?:\s*,\s*0*(?:\d\d?(?:\.\d+)?\s*%|\.\d+\s*%|100(?:\.0*)?\s*%)){2}\s*|(?:rgba[(](?:\s*0*(?:\d\d?(?:\.\d+)?(?:\s*%)?|\.\d+\s*%|100(?:\.0*)?\s*%|(?:1\d\d|2[0-4]\d|25[0-5])(?:\.\d+)?)\s*,){3}|hsla[(]\s*0*(?:[12]?\d{1,2}|3(?:[0-5]\d|60))\s*(?:\s*,\s*0*(?:\d\d?(?:\.\d+)?\s*%|\.\d+\s*%|100(?:\.0*)?\s*%)){2}\s*,)\s*0*(?:\.\d+|1(?:\.0*)?)\s*)[)])$/;
    const queryColor = req.query.color ? req.query.color.trim().replace(/%20/g, '') : '';
    let color = null;
	
    res.set('Access-Control-Allow-Origin', '*');
	
	if (reHex.test(queryColor)) {
		color = Color(queryColor.indexOf('#') === -1 ? '#' + queryColor : queryColor).toCSS().toLowerCase();
	} else if (reAll.test(queryColor)) {
		color = Color(queryColor).toCSS().toLowerCase();
	} else {
        color = 'Invalid color';
    }
	
	res.send(color);
});

app.listen(3000, function() {
    console.log('App listening on port 3000!');
});